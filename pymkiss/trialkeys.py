from datetime import datetime
import time
from time import sleep
import pjlsa
import pyjapc
import pytimber
import pymkitemp.beam_spec
import pymkitemp.rise_time_vs_temperature



# for magnet in pymkitemp.beam_spec.magnets:
#     for beam in pymkitemp.beam_spec.beams:
#         parameter = "MKI.UA%s.TEMP.%s%s/LoggingSetting" % (pymkitemp.beam_spec.beam_to_spec[beam]['mki_name'], magnet, beam)
#         print(parameter)
##     japc.setParam( "MKI.UA23.TEMP.AB1/LoggingSetting", myFields )


japc = pyjapc.PyJapc("LHC.USER.ALL")
japc.rbacLogin("achmieli")

SSend_B2 = int(japc.getParam("MKI.UA87.GEN/SoftStartAcquisition#endTime") ) /1000000000 
SSendOK_B2 = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(SSend_B2))

print(SSendOK_B2)
expectedDuration= int(japc.getParam("MKI.UA23.GEN/SoftStartAcquisition#expectedDuration") ) 

    
print('expectedDuration= ', expectedDuration)