from datetime import datetime
import time
from time import sleep
from multiprocessing import Process
import sys
import getpass
import argparse

import jpype
import pjlsa
import pyjapc
import pytimber

import pymkitemp.beam_spec
import pymkitemp.rise_time_vs_temperature


lsa = pjlsa.LSAClient()
ldb = pytimber.LoggingDB()    

def analyse_SoftStart_end(beam, lastSSendTime):
    log4j_logger.info('Started anaylyse_SoftStart_end ({})'.format(beam))

    field_name = "MKI.UA%s.GEN/SoftStartAcquisition#endTime" % (pymkitemp.beam_spec.beam_to_spec[beam]['mki_name'])
    try:
        SSend = int(japc.getParam(field_name) ) /1000000000
    except:
        log4j_logger.info('Caught exception: {}'.format(sys.exc_info())); 
        return

    SSendOK = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(SSend))
    now=time.time()

    lastSSendTimeStr = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(int(lastSSendTime[0])))
    log4j_logger.info('analyse_SoftStart_end({}): last analysed SoftStart = {}'.format(beam, lastSSendTimeStr))
    log4j_logger.info('analyse_SoftStart_end({}): last executed SoftStart= {}'.format(beam, SSendOK))

    if((SSend > lastSSendTime[0]) and  (now-SSend <60*15)):
        # If last SS is more than 15min in the past, it is ignored
        log4j_logger.debug('analyse_SoftStart_end({}): counter={}, SSendOK={}'.format(beam, counter, SSendOK))
        t1 = str(datetime.fromtimestamp(int(SSend-5*60)))
        t2 = str(datetime.fromtimestamp(int(SSend+30)))

        for magnet in pymkitemp.beam_spec.magnets:
            log4j_logger.debug("analyse_SoftStart_end({}): Start analysis for magnet {}.".format(beam, magnet))
            result=pymkitemp.rise_time_vs_temperature.last_event(ldb, beam, magnet, t1, t2, 'rise_time', 'UP', show_plots=False)
            result2=pymkitemp.rise_time_vs_temperature.last_event(ldb, beam, magnet, t1, t2, 'delay_time', 'DOWN', show_plots=False)
            result3=pymkitemp.rise_time_vs_temperature.last_event(ldb, beam, magnet, t1, t2, 'delay_time_CP', 'DOWN', show_plots=False)
                
            if result and result2 and result3:   
                mean_temp_up=result[0]
                mean_temp_down=result2[0]
                mean_risetime=result[1]
                last_element_time=result[2]*1000000000
                mean_delaytime=result2[1]
                mean_delaytime_CP=result3[1]

                log4j_logger.info('Average rise time= {}'.format(mean_risetime))
                log4j_logger.info('Average delay time= {}'.format(mean_delaytime))
                log4j_logger.info('Average delay time CP= {}'.format(mean_delaytime_CP))
                log4j_logger.info('Average temperature UP= {}'.format(mean_temp_up))
                log4j_logger.info('Average temperature DOWN= {}'.format(mean_temp_down))
                log4j_logger.info("Last element event time: {} ".format(str(datetime.fromtimestamp(result[2]))))
                

                myFields = { 'delay': mean_delaytime, 'delayCP': mean_delaytime_CP, 'risetime': mean_risetime, 'temperatureDown': mean_temp_down, 'temperatureUp': mean_temp_up, 'timestamp': last_element_time }
                name="MKI.UA%s.TEMP.%s%s/LoggingSoftStart" % (pymkitemp.beam_spec.beam_to_spec[beam]['mki_name'], magnet, beam)
                try:
                    log4j_logger.debug('japc.setParam({}), myFields={}): '.format(name, myFields))
                    japc.setParam(name, myFields)
                    lastSSendTime[0] = SSend 
                except:
                    log4j_logger.error('failed to set fields for: ' + name)
                    return
            else: 
                log4j_logger.error('Cannot find event, riseTime result= {}, delay result= {}, delayCP result= {}'.format(result, result2, result3))
    else:
        log4j_logger.info('No new SoftStart found for beam {}'.format(beam))
    log4j_logger.info('End of anaylyse_SoftStart_end ({})'.format(beam))


def parse_cmd():
    parser = argparse.ArgumentParser(description='Analyse MKI SoftStart logged data to extract temperature and risetime of latest pulses.')

    parser.add_argument("--username", help="RBAC user name. If specified the password will be asked. If not specified the RBAC logging is done by location.")
    parser.add_argument("--cfglog", help="Provides the log4j configuration file. If not specified, log4j is disabled.")

    args = parser.parse_args()

    print("args.username= '{}'".format(args.username))
    print("args.cfglog= '{}'".format(args.cfglog))

    return (args.username, args.cfglog)


def config_log4j(log4j_filename):
    log4j = jpype.JPackage('org.apache.log4j')
    if (not log4j_filename == None):
        print("config_log4j(): Configure log4j with file '{}'".format(log4j_filename))
        log4j.PropertyConfigurator.configure(log4j_filename)
    else:
        print("config_log4j(): Remove all appenders")
        log4j.Logger.getRootLogger().removeAllAppenders()

    return log4j


if __name__ == '__main__':
    
    (username, log4j_filename) = parse_cmd()

    log4j = config_log4j(log4j_filename)

    log4j_logger = log4j.LogManager.getLogger("cern.abt.pymkkiss.SoftStartAnalysis")

    password=None
    if (not username == None):
        print("username=", username)
        password = getpass.getpass('Enter Password (hidden if running from command line): ')


    #japc = pyjapc.PyJapc("LHC.USER.ALL")
    japc = pyjapc.PyJapc(selector=None, incaAcceleratorName=None)
    japc.rbacLogin(username, password)

    lsa = pjlsa.LSAClient()
    ldb = pytimber.LoggingDB()    

    lastSSendTime_B1 = [0] # use list to modify this parameter inside the method
    lastSSendTime_B2 = [0]

    counter=0

    while True:
        counter += 1

        log4j_logger.info("START COUNTER: {}".format(counter))

        analyse_SoftStart_end('B1', lastSSendTime_B1)
        analyse_SoftStart_end('B2', lastSSendTime_B2)

        log4j_logger.info("END COUNTER: {}".format(counter))

        sleep(60)

