import getpass
import argparse
from datetime import datetime
import time
from time import sleep

import jpype
import pjlsa
import pyjapc
import pytimber

import pymkitemp.beam_spec
import pymkitemp.rise_time_vs_temperature


def anaylyse_SoftStart_temperatures():
    start = datetime.now()
    now=int(time.time())
    t1 = str(datetime.fromtimestamp(now-120))
    t2 = str(datetime.fromtimestamp(now))
    for beam in pymkitemp.beam_spec.beams:
        try:
            tempLimits = pymkitemp.beam_spec.get_beam_thresholds(beam)
        except:
            log4j_logger.error('Failed to load temperature limits')
            continue
        for magnet in pymkitemp.beam_spec.magnets:
            
            try:
                side="DOWN"
                SISinterlockDown=pymkitemp.beam_spec.get_threshold_fast(magnet, side, tempLimits)          
                relativeDown=pymkitemp.rise_time_vs_temperature.relative_temp(ldb, beam, magnet, side, SISinterlockDown, t1, t2)
                log4j_logger.info("Relative temperature MKI"+magnet+beam+side+": {}".format(relativeDown))
                log4j_logger.info("SIS interlock MKI"+magnet+beam+side+": {}".format(SISinterlockDown))
                side="UP"
                SISinterlockUp=pymkitemp.beam_spec.get_threshold_fast(magnet, side, tempLimits)
                relativeUp=pymkitemp.rise_time_vs_temperature.relative_temp(ldb, beam, magnet, side, SISinterlockUp, t1, t2)
                log4j_logger.info("Relative temperature MKI"+magnet+beam+side+": {}".format(relativeUp))
                log4j_logger.info("SIS interlock MKI"+magnet+beam+side+": {}".format(SISinterlockUp))
            except:
                log4j_logger.error('Caught exception')
                continue
            myFields = {
                'temperatureLimitDown': SISinterlockDown,
                'temperatureLimitUp': SISinterlockUp,
                'temperatureRelDown': relativeDown,
                'temperatureRelUp': relativeUp,
                'timestamp': time.time()*1000000000,
            }
            name="MKI.UA%s.TEMP.%s%s/LoggingTemperatures" % (pymkitemp.beam_spec.beam_to_spec[beam]['mki_name'], magnet, beam)
            log4j_logger.debug("name= " + name)
            try:
                log4j_logger.debug("Call japc.setParam({})) with fields:\n{}".format(name, myFields))
                japc.setParam(name, myFields)
            except:
                log4j_logger.error('Failed to save fields for name= {}'.format(name))
                continue


def parse_cmd():
    parser = argparse.ArgumentParser(description='Analyse MKI SoftStart temperatures.')

    parser.add_argument("--username", help="RBAC user name. If specified the password will be asked. If not specified the RBAC logging is done by location.")
    parser.add_argument("--cfglog", help="Provides the log4j configuration file. If not specified, log4j is disabled.")

    args = parser.parse_args()

    print("args.username= '{}'".format(args.username))
    print("args.cfglog= '{}'".format(args.cfglog))

    return (args.username, args.cfglog)


def config_log4j(log4j_filename):
    log4j = jpype.JPackage('org.apache.log4j')

    print("config_log4j(): Remove all appenders")
    log4j.Logger.getRootLogger().removeAllAppenders()
    log4j.Logger.getRootLogger().setLevel(log4j.Level.OFF)

    if (not log4j_filename == None):
        print("config_log4j(): Configure log4j with file '{}'".format(log4j_filename))
        log4j.PropertyConfigurator.configure(log4j_filename)

    return log4j


if __name__ == '__main__':

    (username, log4j_filename) = parse_cmd()

    log4j = config_log4j(log4j_filename)

    log4j_logger = log4j.LogManager.getLogger("cern.abt.pymkkiss.SoftStartAnalysis")

    password=None
    if (not username == None):
        print("username=", username)
        password = getpass.getpass('Enter Password (hidden if running from command line): ')


    japc = pyjapc.PyJapc(selector=None, incaAcceleratorName=None)
    japc.rbacLogin(username, password)
 
    lsa = pjlsa.LSAClient()
    ldb = pytimber.LoggingDB()

    counter=0
    while True:
        counter += 1

        log4j_logger.info("START COUNTER: {}".format(counter))

        anaylyse_SoftStart_temperatures()

        log4j_logger.info("END COUNTER: {}".format(counter))

        sleep(60)

