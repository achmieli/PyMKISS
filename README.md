# PyMKISS

Real-time analysis of the Soft Start end:
- Average risetime
- Average delay time
- Average delay CP time
- Average temperature at MKI upstream end
- Average temperature at MKI downstream end
- Timestamp of the last event

Polls PyTimber, PjLSA, PyJAPC every 60 seconds.

Monitoring of SIS interlocks and relative temperatures.

## Installation

Available only with access to the CERN technical network
```
pip install git+https://gitlab.cern.ch/scripting-tools/cmmnbuild-dep-manager.git
pip install git+https://gitlab.cern.ch/scripting-tools/pyjapc.git
pip install pytimber
pip install pjlsa
pip install git+https://gitlab.cern.ch/achmieli/PyMKITemp.git
git clone https://gitlab.cern.ch/achmieli/PyMKISS.git
cd PyMKISS
```

## Updating external packages

If changes were made to any of the above packages. Re-run the pip installs.
For example, if PyMKITemp has changed, run:
```
pip install git+https://gitlab.cern.ch/achmieli/PyMKITemp.git -U
```

### Getting Started

For Soft Start analysis (will prompt for username and password):
```
python pymkiss/SoftStartAnalysis.py
```

For SIS interlock and relative temperature monitoring:
```
python pymkiss/SIStemperatures.py
```
